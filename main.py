import sys
import random
import pygame

from pygame.locals import *
from locals import *


def terminate():
    pygame.quit()
    sys.exit()


def getStartingBoard():
    board = []

    for x in range(BOARD_WIDTH):
        column = []
        for y in range(BOARD_HEIGHT):
            column.append(x + y*BOARD_WIDTH + 1)
        board.append(column)

    board[BOARD_WIDTH-1][BOARD_HEIGHT-1] = BLANK

    return board


def getBlankPosition(board):
    for x in range(BOARD_WIDTH):
        for y in range(BOARD_HEIGHT):
            if board[x][y] == BLANK:
                return (x, y)


def makeMove(board, move):
    blank_x, blank_y = getBlankPosition(board)

    if move == UP:
        board[blank_x][blank_y], board[blank_x][blank_y + 1] = board[blank_x][blank_y + 1], board[blank_x][blank_y]
    elif move == DOWN:
        board[blank_x][blank_y], board[blank_x][blank_y - 1] = board[blank_x][blank_y - 1], board[blank_x][blank_y]
    elif move == RIGHT:
        board[blank_x][blank_y], board[blank_x - 1][blank_y] = board[blank_x - 1][blank_y], board[blank_x][blank_y]
    elif move == LEFT:
        board[blank_x][blank_y], board[blank_x + 1][blank_y] = board[blank_x + 1][blank_y], board[blank_x][blank_y]


def isValidMove(board, move):
    blank_x, blank_y = getBlankPosition(board)

    return (move == UP and blank_y < BOARD_HEIGHT - 1) or \
        (move == DOWN and blank_y > 0) or \
        (move == LEFT and  blank_x < BOARD_WIDTH - 1) or \
        (move == RIGHT and blank_x > 0)


def getRandomMove(board, lastMove=None):
    validMoves = [UP, DOWN, LEFT, RIGHT]

    if lastMove == UP or not isValidMove(board, DOWN):
        validMoves.remove(DOWN)
    if lastMove == DOWN or not isValidMove(board, UP):
        validMoves.remove(UP)
    if lastMove == LEFT or not isValidMove(board, RIGHT):
        validMoves.remove(RIGHT)
    if lastMove == RIGHT or not isValidMove(board, LEFT):
        validMoves.remove(LEFT)

    return random.choice(validMoves)


def getLeftTopOfTile(tile_x, tile_y):
    left = X_MARGIN + (tile_x * TILE_SIZE) + (tile_x - 1)
    top = Y_MARGIN + (tile_y * TILE_SIZE) + (tile_y - 1)

    return (left, top)


def getSpotClicked(board, x, y):
    for tile_x in range(BOARD_WIDTH):
        for tile_y in range(BOARD_HEIGHT):
            left, top = getLeftTopOfTile(tile_x, tile_y)
            rect = pygame.Rect(left, top, TILE_SIZE, TILE_SIZE)
            if rect.collidepoint(x, y):
                return (tile_x, tile_y)


def drawTile(tile_x, tile_y, number, adj_x=0, adj_y=0):
    left, top = getLeftTopOfTile(tile_x, tile_y)

    pygame.draw.rect(DISPLAY, TILE_COLOR, (left + adj_x, top + adj_y, TILE_SIZE, TILE_SIZE))

    textDisplay = BASIC_FONT.render(str(number), True, TEXT_COLOR)
    textRect = textDisplay.get_rect()

    textRect.center = (left + adj_x + TILE_SIZE // 2, top + adj_y + TILE_SIZE // 2)

    DISPLAY.blit(textDisplay, textRect)


def makeText(text, color, bgcolor, top, left):
    textDisplay = BASIC_FONT.render(text, True, color, bgcolor)
    textRect = textDisplay.get_rect()

    textRect.topleft = (top, left)

    return (textDisplay, textRect)


def drawBoard(board, message):
    DISPLAY.fill(BG_COLOR)

    if message:
        textDisplay, textRect = makeText(message, MESSAGE_COLOR, BG_COLOR, 5, 5)
        DISPLAY.blit(textDisplay, textRect)

    for tile_x in range(BOARD_WIDTH):
        for tile_y in range(BOARD_HEIGHT):
            if board[tile_x][tile_y]:
                drawTile(tile_x, tile_y, board[tile_x][tile_y])

    left, top = getLeftTopOfTile(0, 0)

    width = BOARD_WIDTH * TILE_SIZE
    height = BOARD_HEIGHT * TILE_SIZE

    pygame.draw.rect(DISPLAY, BORDER_COLOR, (left - 5, top - 5, width + 11, height + 11), 4)

    DISPLAY.blit(RESET_DISPLAY, RESET_RECT)
    DISPLAY.blit(NEW_DISPLAY, NEW_RECT)
    DISPLAY.blit(SOLVE_DISPLAY, SOLVE_RECT)


def slideAnimation(board, direction, message, animationSpeed):
    blank_x, blank_y = getBlankPosition(board)

    if direction == UP:
        move_x, move_y = blank_x, blank_y + 1
    elif direction == DOWN:
        move_x, move_y = blank_x, blank_y - 1
    elif direction == LEFT:
        move_x, move_y = blank_x + 1, blank_y
    elif direction == RIGHT:
        move_x, move_y = blank_x - 1, blank_y

    drawBoard(board, message)

    baseDisplay = DISPLAY.copy()
    moveLeft, moveTop = getLeftTopOfTile(move_x, move_y)

    pygame.draw.rect(baseDisplay, BG_COLOR, (moveLeft, moveTop, TILE_SIZE, TILE_SIZE))

    for i in range(0, TILE_SIZE, animationSpeed):
        checkForQuit()

        DISPLAY.blit(baseDisplay, (0, 0))

        if direction == UP:
            drawTile(move_x, move_y, board[move_x][move_y], 0, -i)
        elif direction == DOWN:
            drawTile(move_x, move_y, board[move_x][move_y], 0, i)
        elif direction == LEFT:
            drawTile(move_x, move_y, board[move_x][move_y], -i, 0)
        elif direction == RIGHT:
            drawTile(move_x, move_y, board[move_x][move_y], i, 0)

        pygame.display.update()
        FPS_CLOCK.tick(FPS)


def generateNewPuzzle(numSlides):
    sequence = []
    board = getStartingBoard()

    drawBoard(board, '')

    pygame.display.update()
    pygame.time.wait(500)

    lastMove = None

    for i in range(numSlides):
        move = getRandomMove(board, lastMove)
        slideAnimation(board, move, 'Generating new puzzle...', TILE_SIZE // 3)

        makeMove(board, move)
        sequence.append(move)

        lastMove = move

    return (board, sequence)


def resetAnimation(board, allMoves):
    revAllMoves = allMoves[::-1]

    for move in revAllMoves:
        if move == UP:
            oppositeMove = DOWN
        elif move == DOWN:
            oppositeMove = UP
        elif move == LEFT:
            oppositeMove = RIGHT
        elif move == RIGHT:
            oppositeMove = LEFT

        slideAnimation(board, oppositeMove, '', TILE_SIZE // 2)
        makeMove(board, oppositeMove)


def checkForQuit():
    for event in pygame.event.get(QUIT):
        terminate()

    for event in pygame.event.get(KEYUP):
        if event.key == K_ESCAPE:
            terminate()
        pygame.event.post(event)


def startGame():
    mainBoard, solutionSeq = generateNewPuzzle(80)

    SOLVED_BOARD = getStartingBoard()

    allMoves = []

    while True:
        slideTo = None
        message = ''

        if mainBoard == SOLVED_BOARD:
            message = 'Solved!'

        drawBoard(mainBoard, message)

        checkForQuit()

        for event in pygame.event.get():
            if event.type == MOUSEBUTTONUP:
                spot = getSpotClicked(mainBoard, event.pos[0], event.pos[1])

                if spot == None:
                    if RESET_RECT.collidepoint(event.pos[0], event.pos[1]):
                        resetAnimation(mainBoard, allMoves)
                        allMoves = []
                    elif NEW_RECT.collidepoint(event.pos[0], event.pos[1]):
                        mainBoard, solutionSeq = generateNewPuzzle(80)
                        allMoves = []
                    elif SOLVE_RECT.collidepoint(event.pos[0], event.pos[1]):
                        resetAnimation(mainBoard, solutionSeq + allMoves)
                        allMoves = []
                else:
                    blank_x, blank_y = getBlankPosition(mainBoard)

                    if spot[0] == blank_x+1 and spot[1] == blank_y:
                        slideTo = LEFT
                    elif spot[0] == blank_x-1 and spot[1] == blank_y:
                        slideTo = RIGHT
                    elif spot[0] == blank_x and spot[1] == blank_y+1:
                        slideTo = UP
                    elif spot[0] == blank_x and spot[1] == blank_y-1:
                        slideTo = DOWN

            elif event.type == KEYUP:
                if event.key in (K_LEFT, K_a) and isValidMove(mainBoard, LEFT):
                    slideTo = LEFT
                elif event.key in (K_RIGHT, K_d) and isValidMove(mainBoard, RIGHT):
                    slideTo = RIGHT
                elif event.key in (K_UP, K_w) and isValidMove(mainBoard, UP):
                    slideTo = UP
                elif event.key in (K_DOWN, K_s) and isValidMove(mainBoard, DOWN):
                    slideTo = DOWN

        if slideTo:
            slideAnimation(mainBoard, slideTo, 'Click tile or press arrowkeys to slide.', 8)
            makeMove(mainBoard, slideTo)

            allMoves.append(slideTo)

        pygame.display.update()
        FPS_CLOCK.tick(FPS)


def main():
    global FPS_CLOCK, BASIC_FONT, \
        DISPLAY, RESET_DISPLAY, NEW_DISPLAY, SOLVE_DISPLAY, \
        RESET_RECT, NEW_RECT, SOLVE_RECT

    pygame.init()

    FPS_CLOCK = pygame.time.Clock()

    DISPLAY = pygame.display.set_mode(DISPLAY_SIZE)
    pygame.display.set_caption(DISPLAY_CAPTION)

    BASIC_FONT = pygame.font.Font('freesansbold.ttf', BASIC_FONT_SIZE)

    RESET_DISPLAY, RESET_RECT = makeText('Reset', TEXT_COLOR, TILE_COLOR, DISPLAY_SIZE[0] - 120, DISPLAY_SIZE[1] - 90)
    NEW_DISPLAY, NEW_RECT = makeText('New Game', TEXT_COLOR, TILE_COLOR, DISPLAY_SIZE[0] - 120, DISPLAY_SIZE[1] - 60)
    SOLVE_DISPLAY, SOLVE_RECT = makeText('Solve', TEXT_COLOR, TILE_COLOR, DISPLAY_SIZE[0] - 120, DISPLAY_SIZE[1] - 30)

    startGame()


if __name__ == '__main__':
    main()
